const input = document.getElementById('input-date');
const rootDiv = document.getElementById('root');
function Calendar(input) {
    this.now = new Date();
    this.year = this.now.getFullYear();
    this.month = this.now.getMonth();
    this.day = this.now.getDate();
    this.divContainer = null;
    this.divButtons = null;
    this.divDate = null;
    this.divHeader = null;
    this.divCalendarTable = null;
    this.divCalendarContainer = null;
    this.divTime = null;
    this.timeViewTimer = null;
    this.input = input;
    this.selectedDay = null;


    this.init = function() {
        this.divContainer = document.createElement('div');
        this.divContainer.classList.add('calendar');

        this.divTime = document.createElement('div');
        this.divTime.classList.add('time-container');
        this.createTimeView();

        this.divButtons = document.createElement('div');
        this.divButtons.classList.add('btn-container');
        this.createButtons();

        this.divDate = document.createElement('div');
        this.divDate.classList.add('date-text');

        this.divHeader = document.createElement('div');
        this.divHeader.classList.add('calendar-header');
        this.createDateText();

        this.divHeader.appendChild(this.divDate);
        this.divHeader.appendChild(this.divButtons);

        this.divCalendarContainer = document.createElement('div');
        this.createCalendarTable();
        this.divCalendarContainer.appendChild(this.divCalendarTable);


        this.divContainer.appendChild(this.divTime);
        this.divContainer.appendChild(this.divHeader);
        this.divContainer.appendChild(this.divCalendarContainer);
        rootDiv.appendChild(this.divContainer);

        this.divContainer.addEventListener('click', function(e) {
            e.stopImmediatePropagation();
        });
        
    }

    this.createTimeView = function() {
        const timeContainer = document.createElement('div');
        timeContainer.classList.add('time-text');
        timeContainer.innerHTML = this.now.getHours() + ":" + this.now.getMinutes() + ':' + this.now.getSeconds();
        
        timeViewTimer = setInterval(function () {
            const date = new Date();
            timeContainer.innerHTML = date.getHours() + ":" + date.getMinutes() + ':' + date.getSeconds();
        }, 1000);

        const dataInfoContainer = document.createElement('div');
        dataInfoContainer.classList.add('data-info-container');
        dataInfoContainer.innerHTML = this.getDayName(this.now.getDay()-1) + ', ' + this.day + ' ' + this.getMonthName(this.month) + ' ' + this.year;
        

        this.divTime.appendChild(timeContainer);
        this.divTime.appendChild(dataInfoContainer);
    }

    this.createButtons = function() {
        const buttonPrev  = document.createElement('button');
        buttonPrev.innerHTML = '&#8679';
        buttonPrev.classList.add('btn');
        buttonPrev.addEventListener('click', function() {
            this.month--;
            if(this.month <= 0) {
                this.month = 12;
                this.year--;
            }
            this.createDateText();
            this.createCalendarTable();
        }.bind(this));

        const buttonNext = document.createElement('button');
        buttonNext.innerHTML = '&#8681';
        buttonNext.classList.add('btn');
        buttonNext.addEventListener('click', function() {
            this.month++;
            if(this.month > 12) {
                this.month = 1;
                this.year++;
            }
            this.createDateText();
            this.createCalendarTable();
        }.bind(this));

        this.divButtons.appendChild(buttonPrev);
        this.divButtons.appendChild(buttonNext);
    }

    this.getMonthName = function(month = this.month) {
        const monthNames = ['styczeń', 'luty', 'marzec', 'kwiecień', 'maj', 'czerwiec', 'lipiec', 'sierpień', 'wrzesień', 'październik', 'listopad', 'grudzień'];
        return monthNames[month];
    }

    this.getDayName = function(day) {
        const dayNames = ['Poniedzialek', 'Wtorek', 'Sroda', 'Czwartek', 'Piatek', 'Sobota', 'Niedziela'];
        return dayNames[day];
    }

    this.createDateText = function() {
        this.divDate.innerHTML = this.getMonthName() + ' ' + this.year;
    }

    this.createDayList = function(table) {
        const tr = document.createElement('tr');
        const days = ['Pon', 'Wto', 'Śro', 'Czw', 'Pią', 'Sob', 'Nie'];
        for(var i=0; i<days.length; i++) {
            const td = document.createElement('td');
            td.innerHTML = days[i];
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }

    this.getDaysInMonth = function(month, year){
        if(month < 0) month = 12;
        return new Date(year, month, 0).getDate();
    }

    this.createCalendarTable = function() {
        this.divCalendarContainer.innerHTML = '';
        this.divCalendarTable = document.createElement('table');
        this.divCalendarTable.classList.add('calendar-table')
        this.createDayList(this.divCalendarTable);
        this.createDaysTable();
        this.divCalendarContainer.appendChild(this.divCalendarTable);

    }

    this.getFirstAndLastDayOfMonth = function() {
        var firstDay = new Date(this.year, this.month, 1).getDay();
        if(firstDay === 0) firstDay = 7;

        const lastDay = this.getDaysInMonth(this.year, this.month);
        return {firstDay, lastDay};
    }

    this.createDaysTable = function() {
        const firstDay = this.getFirstAndLastDayOfMonth().firstDay;
        const lastDay = this.getFirstAndLastDayOfMonth().lastDay;
        const lastDayPrevMonth = this.getDaysInMonth(this.month, this.year);
        let monthDays = 1;
        let isFirstDaySet = false;
        let nextMonth = false;

        for(var i=0; i<6; i++) {
            const tr = document.createElement('tr');

            for(var j=1; j<=7; j++) {
                const td = document.createElement('td');
                if(!isFirstDaySet && firstDay === j) {
                    td.innerHTML = monthDays;
                    isFirstDaySet = true;
                    td.value = monthDays;
                } else if(isFirstDaySet) {
                    monthDays++;
                    if(monthDays > lastDay) {
                        monthDays = 1;
                        nextMonth = true;
                    }

                    if(nextMonth == true) {
                        td.classList.add('gray');
                    } else td.value = monthDays;


                    td.innerHTML = monthDays;
                } else {
                    td.innerHTML = lastDayPrevMonth - (firstDay-1) + j;
                    td.classList.add('gray');
                }
                if(this.selectedDay !== null)
                    if( this.selectedDay.month === this.month &&
                        this.selectedDay.day === monthDays && 
                        this.selectedDay.year === this.year) tr.classList.add('selected');
                tr.appendChild(td);
            }
            this.divCalendarTable.appendChild(tr);
        }
        this.bindTableDays();
    }

    this.bindTableDays = function() {
        this.divCalendarTable.addEventListener('click', function(e) {
            if (e.target.tagName.toLowerCase() === 'td' && typeof e.target.value === 'number') {
                if(document.getElementsByClassName('selected')[0])
                    document.getElementsByClassName('selected')[0].classList.remove('selected');

                this.selectedDay = {month: this.month, year: this.year, day: e.target.value}
                this.input.value = e.target.value + '/' + this.month + "/" + this.year;
                e.target.classList.add('selected');
            }
        }.bind(this));
    }

    this.show = function() {
        this.divContainer.classList.add('show');
    }

    this.hide = function() {
        this.divContainer.classList.remove('show');
    }

    this.input.addEventListener('click', function(e) {
        e.stopImmediatePropagation();
        this.show();
    }.bind(this));

    document.addEventListener('click', function() {
        this.hide();
    }.bind(this));
}
const cal = new Calendar(input);
cal.init();